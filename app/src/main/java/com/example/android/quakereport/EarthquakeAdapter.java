package com.example.android.quakereport;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class EarthquakeAdapter extends ArrayAdapter<Earthquake>
{
    public EarthquakeAdapter(Activity context, ArrayList<Earthquake> earthquakes)
    {
        super(context, 0, earthquakes);
    }

    @Override
    public View getView(int position, View transformView, ViewGroup parentGroup)
    {
        // Create new list item
        View _listItemView = transformView;
        if (_listItemView == null)
        {
            _listItemView = LayoutInflater.from(getContext()).inflate(R.layout.earthquake_layout, parentGroup, false);
        }

        // Get current item position
        Earthquake _currentEarthquake = getItem(position);

        // Get current list item magnitude
        Double _magnitude = _currentEarthquake.getMagnitude();

        // Update magnitude text view
        TextView magnitudeText = (TextView) _listItemView.findViewById(R.id.magnitude_text_view);
        magnitudeText.setText(Double.toString(Math.round(_magnitude * 10) / 10)); // Round the number to one digit after dot format

        /* TODO Set the right shape color based on the earthquake magnitude */
        //Updates magnitude background circle color
        GradientDrawable magnitudeDrawable = (GradientDrawable) magnitudeText.getBackground();
        _magnitude = Math.floor(_magnitude);
        int mMagnitudeFloor = _magnitude.intValue();

        System.out.println(mMagnitudeFloor);
        int mColorCode;
        switch (mMagnitudeFloor)
        {
            case 1:
                mColorCode = R.color.magnitude1;
                break;
            case 2:
                mColorCode = R.color.magnitude2;
                break;
            case 3:
                mColorCode = R.color.magnitude3;
                break;
            case 4:
                mColorCode = R.color.magnitude4;
                break;
            case 5:
                mColorCode = R.color.magnitude5;
                break;
            case 6:
                mColorCode = R.color.magnitude6;
                break;
            case 7:
                mColorCode = R.color.magnitude7;
                break;
            case 8:
                mColorCode = R.color.magnitude8;
                break;
            case 9:
                mColorCode = R.color.magnitude9;
                break;
            default:
                mColorCode = R.color.magnitude10plus;
                break;
        }

        System.out.println("!!Final Color code as an integer " + mColorCode);

        // Set magnitude color into magnitude drawable
        magnitudeDrawable.setColor(mColorCode);

        // Set a random color to magnitude drawable
        //magnitudeDrawable.setColor(new Random().nextInt());

        // Proximity text view
        TextView proximityText = (TextView) _listItemView.findViewById(R.id.proximity_text_view);
        proximityText.setText(_currentEarthquake.getProximity());

        // Location text view
        TextView locationText = (TextView) _listItemView.findViewById(R.id.location_text_view);
        locationText.setText(_currentEarthquake.getLocation());

        // Date text view
        TextView dateText = (TextView) _listItemView.findViewById(R.id.date_text_view);
        dateText.setText(_currentEarthquake.getDate());

        // Time text view
        TextView timeText = (TextView) _listItemView.findViewById(R.id.time_text_view);
        timeText.setText(_currentEarthquake.getTime());

        // Return list item
        return _listItemView;
    }
}
