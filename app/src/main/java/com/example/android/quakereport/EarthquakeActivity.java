/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.quakereport;

import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class EarthquakeActivity extends AppCompatActivity implements LoaderCallbacks<List<Earthquake>>, SharedPreferences.OnSharedPreferenceChangeListener
{
    private static final String LOG_TAG = EarthquakeActivity.class.getName();
    private static final int EARTHQUAKE_LOADER_ID = 1;
    protected static String EARTHQUAKE_URL = "https://earthquake.usgs.gov/fdsnws/event/1/query"; // ?format=geojson&eventtype=earthquake&orderby=time&minmag=2&limit=10

    // Layout components reference
    private EarthquakeAdapter earthquakeAdapter;
    private TextView emptyListTextView;
    private ListView earthquakeListView;
    private ProgressBar loadingProgressCircle;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earthquake_activity);

        // Reference to the earthquake list view
        earthquakeListView = (ListView) findViewById(R.id.earthquake_list_view);

        // Reference to empty earthquake text view and set is as the default empty state
        emptyListTextView = (TextView) findViewById(R.id.empty_text_view);
        earthquakeListView.setEmptyView(emptyListTextView);

        // Reference to progress bar
        loadingProgressCircle = (ProgressBar) findViewById(R.id.list_loading_circle);

        // Check network connection status
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        prefs.registerOnSharedPreferenceChangeListener(this);

        // If there is a network available
        if (networkInfo != null && networkInfo.isConnected())
        {
            // Fetch earthquake data
            LoaderManager loaderManager = getLoaderManager();
            loaderManager.initLoader(EARTHQUAKE_LOADER_ID, null, this);
        }
        else
        {
            // Display that there is no network available
            emptyListTextView.setText(R.string.network_not_found);
            loadingProgressCircle.setVisibility(View.GONE);
        }

        // Earthquake list adapter
        earthquakeAdapter = new EarthquakeAdapter(this, new ArrayList<Earthquake>());

        // Link earthquake adapter to the earthquake list
        earthquakeListView.setAdapter(earthquakeAdapter);

        // Earthquake intents
        earthquakeListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                // Find the current earthquake that was clicked on
                Earthquake currentEarthquake = earthquakeAdapter.getItem(position);

                // Get uri url and parse it
                Uri uriEarthquake = Uri.parse(currentEarthquake.getUrl());

                // Define new intent to a website page
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, uriEarthquake);

                // Start the activity with the new intent
                startActivity(websiteIntent);
            }
        });
    }

    @Override
    public Loader<List<Earthquake>> onCreateLoader(int i, Bundle bundle)
    {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String minMagnitude = sharedPrefs.getString(getString(R.string.settings_min_magnitude_key), getString(R.string.settings_min_magnitude_default));
        String orderBy = sharedPrefs.getString(getString(R.string.settings_order_by_key), getString(R.string.settings_order_by_default));

        Uri baseUri = Uri.parse(EARTHQUAKE_URL);
        Uri.Builder uriBuilder = baseUri.buildUpon();

        uriBuilder.appendQueryParameter("format", "geojson");
        uriBuilder.appendQueryParameter("limit", "10");
        uriBuilder.appendQueryParameter("minmag", minMagnitude);
        uriBuilder.appendQueryParameter("orderby", orderBy);

        return new EarthquakeLoader(this, uriBuilder.toString());
    }

    @Override
    public void onLoadFinished(Loader<List<Earthquake>> loader, List<Earthquake> earthquakes)
    {
        // Hide loading indicator because the data has been loaded
        View loadingIndicator = findViewById(R.id.list_loading_circle);
        loadingIndicator.setVisibility(View.GONE);

        // Set empty state text to display "No earthquakes found."
        emptyListTextView.setText(R.string.empty_list);

        // Clear the adapter of previous earthquake data

        // If there is a valid list of {@link Earthquake}s, then add them to the adapter's data set. This will trigger the ListView to update.
        if (earthquakes != null && !earthquakes.isEmpty())
        {
            earthquakeAdapter.addAll(earthquakes);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences prefs, String key)
    {
        if (key.equals(getString(R.string.settings_min_magnitude_key)) || key.equals(getString(R.string.settings_order_by_key)))
        {
            // Clear the ListView as a new query will be kicked off
            earthquakeAdapter.clear();

            // Hide the empty state text view as the loading indicator will be displayed
            emptyListTextView.setVisibility(View.GONE);

            // Show the loading indicator while new data is being fetched
            View loadingIndicator = findViewById(R.id.list_loading_circle);
            loadingIndicator.setVisibility(View.VISIBLE);

            // Restart the loader to requery the USGS as the query settings have been updated
            getLoaderManager().restartLoader(EARTHQUAKE_LOADER_ID, null, this);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Earthquake>> loader)
    {
        earthquakeAdapter.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.action_settings)
        {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
