package com.example.android.quakereport;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EarthquakeUtils
{
    private static final String LOG_TAG = EarthquakeUtils.class.getSimpleName();

    // Timeout in milliseconds
    private static final int CONNECTION_TIME_OUT = 30000;
    private static final int READ_TIME_OUT = 20000;

    // Create url object from url location
    private static URL createURL(String stringUrl)
    {
        URL url = null;

        try
        {
            url = new URL (stringUrl);
        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
        }

        return url;
    }

    // Establish a connection with the requested url
    private static String makeHttpRequest(URL url) throws IOException
    {
        String jsonResponse = "";

        // If the URL is null, then return early.
        if (url == null) {
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(READ_TIME_OUT);
            urlConnection.setConnectTimeout(CONNECTION_TIME_OUT);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // If the request was successful (response code 200),
            // then read the input stream and parse the response.
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the earthquake JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                // Closing the input stream could throw an IOException, which is why
                // the makeHttpRequest(URL url) method signature specifies than an IOException
                // could be thrown.
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    // Converts the inputString into a String
    private static String readFromStream(InputStream inputStream) throws IOException
    {
        StringBuilder output = new StringBuilder();

        if (inputStream != null)
        {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);

            String line = reader.readLine();

            while (line != null){
                output.append(line);
                line = reader.readLine();
            }
        }

        // Return the inputStream formatted
        return output.toString();
    }

    //
    public static List<Earthquake> fetchEarthquakeData(String requestedURL)
    {
        // Create url object
        URL url = createURL(requestedURL);

        String jsonResponse = null;

        // Fetch json response
        try
        {
            jsonResponse = makeHttpRequest(url);
        }
        catch (IOException e)
        {
            Log.e(LOG_TAG, " Problem making http request ", e);
        }

        // Extract features from json and store in a list
        List<Earthquake> earthquakesResponse = extractFeatureFromJSON(jsonResponse);

        // Return the response
        return earthquakesResponse;
    }

    // Extract json features
    public static List<Earthquake> extractFeatureFromJSON(String jsonString)
    {
        // Create a list of earthquake locations
        ArrayList<Earthquake> mEarthquakeList = new ArrayList<>();

        // Parse json
        if (jsonString == null && TextUtils.isEmpty(jsonString))
        {
            Log.w("EarthquakeActivity ", "JSON file is empty");
            return null;
        }
        else
        {
            try
            {
                // Create json object
                JSONObject reportObject = new JSONObject(jsonString);

                // Get features from json Object
                JSONArray featuresArray = reportObject.getJSONArray("features");

                // Loop through each feature
                for (int feature = 0; feature < featuresArray.length(); feature++)
                {
                    JSONObject propertiesObject = featuresArray.getJSONObject(feature).getJSONObject("properties");

                    // Get magnitude and store
                    Double mMagnitude = propertiesObject.getDouble("mag");

                    // Get place and split proximity and location and store them
                    String mPlace = propertiesObject.getString("place");
                    int index = mPlace.indexOf(" of ");
                    String[] mSplit = mPlace.split(" of ");
                    String mProximity = mSplit[0] + " of ";
                    String mLocation = mPlace.substring(index + 4);

                    // Format date and store
                    Date mDateObject = new Date(propertiesObject.getLong("time"));
                    SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                    String mDate = mSimpleDateFormat.format(mDateObject);
                    mSimpleDateFormat = new SimpleDateFormat("hh:mm");
                    String mTime = mSimpleDateFormat.format(mDateObject);

                    // Get url
                    String mUrl = propertiesObject.getString("url");

                    // Add an earthquake item in the list
                    mEarthquakeList.add(new Earthquake(mMagnitude, mProximity, mLocation, mDate, mTime, mUrl));
                }
            }
            catch (JSONException stringException)
            {
                Log.e("EarthquakeActivity ", "JSONException jsonString " + stringException.getMessage());
            }
        }

        // Return a list with every earthquake found
        return  mEarthquakeList;
    }
}
